package ru.t1.kharitonova.tm.api;

import ru.t1.kharitonova.tm.model.Project;

public interface IProjectService extends IProjectRepository {
    Project create(String name, String description);
}
