package ru.t1.kharitonova.tm.api;

import ru.t1.kharitonova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    List<Project> findAll();

    void clear();
}
