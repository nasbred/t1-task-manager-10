package ru.t1.kharitonova.tm.api;

import ru.t1.kharitonova.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    Task create(String name, String description);
}
