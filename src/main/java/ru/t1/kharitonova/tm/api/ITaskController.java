package ru.t1.kharitonova.tm.api;

public interface ITaskController {
    void createTask();

    void showTasks();

    void clearTasks();
}
