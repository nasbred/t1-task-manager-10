package ru.t1.kharitonova.tm.service;

import ru.t1.kharitonova.tm.api.ICommandRepository;
import ru.t1.kharitonova.tm.api.ICommandService;
import ru.t1.kharitonova.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
