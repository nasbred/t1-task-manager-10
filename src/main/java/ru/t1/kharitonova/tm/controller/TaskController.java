package ru.t1.kharitonova.tm.controller;

import ru.t1.kharitonova.tm.api.ITaskService;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ru.t1.kharitonova.tm.api.ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks){
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASKS CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
