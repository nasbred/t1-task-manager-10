package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.ICommandRepository;
import ru.t1.kharitonova.tm.constant.ArgumentConst;
import ru.t1.kharitonova.tm.constant.CommandConst;
import ru.t1.kharitonova.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Display developer info.");
    public static final Command VERSION = new Command(CommandConst.VERSION,ArgumentConst.VERSION, "Display program version.");
    public static final Command HELP = new Command(CommandConst.HELP,ArgumentConst.HELP, "Display list of terminal commands.");
    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");
    public static final Command INFO = new Command(CommandConst.INFO,ArgumentConst.INFO, "Display system information.");
    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS,ArgumentConst.ARGUMENTS, "Display argument list.");
    public static final Command COMMANDS = new Command(CommandConst.COMMANDS,ArgumentConst.COMMANDS, "Display command list.");
    public static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST,null, "Display list projects.");
    public static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE,null, "Create new project.");
    public static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR,null, "Remove all projects.");
    public static final Command TASK_LIST = new Command(CommandConst.TASK_LIST,null, "Display list tasks.");
    public static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE,null, "Create new task.");
    public static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR,null, "Remove all tasks.");
    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS, EXIT
    };

    public Command[] getTerminalCommands(){
        return TERMINAL_COMMANDS;
    }

}
