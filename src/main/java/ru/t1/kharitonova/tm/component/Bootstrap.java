package ru.t1.kharitonova.tm.component;

import ru.t1.kharitonova.tm.api.*;
import ru.t1.kharitonova.tm.constant.ArgumentConst;
import ru.t1.kharitonova.tm.constant.CommandConst;
import ru.t1.kharitonova.tm.controller.CommandController;
import ru.t1.kharitonova.tm.controller.ProjectController;
import ru.t1.kharitonova.tm.controller.TaskController;
import ru.t1.kharitonova.tm.repository.CommandRepository;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.service.CommandService;
import ru.t1.kharitonova.tm.service.ProjectService;
import ru.t1.kharitonova.tm.service.TaskService;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        runArguments(args);
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            runCommand(TerminalUtil.nextLine());
        }
    }

    private void runArguments(final String[] arg){
        if(arg == null || arg.length == 0) return;
        for (String s : arg) {
            runArgument(s);
        }
        System.exit(0);
    }

    private void runCommand(final String param){
        switch (param) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void runArgument(final String param){
        switch (param) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
        }
    }

    public void exit() {
        System.exit(0);
    }

}
